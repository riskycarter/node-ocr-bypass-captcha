# node-ocr-bypass-captcha
Using "node-tesseract-ocr" and "puppeteer" to bypass captcha from image e.g. ( .jpg , .png) <br />
Require to install **Tesseract**  : https://tesseract-ocr.github.io/tessdoc/Downloads
## Installation
```bash
npm install puppeteer https fs node-tesseract-ocr
```

## Run Node
```bash
node main.js 
```



## Run Node with Mode
**MODE : 1** = Download image, **2** = Screenshot
```bash
// Run with mode 1 
node main.js 1
```
```bash
// Run with mode 2 
node main.js 2
```
