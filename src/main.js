const puppeteer = require('puppeteer');
const fs = require('fs');
const https = require('https');
const tesseract = require("node-tesseract-ocr")

/* ============================================================
      Set whitelist for captcha
============================================================ */
const char_lower_case = 'abcdefghijklmnopqrstuvwxyz'
const char_upper_case = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const num_case = '1234567890'

/* ============================================================
  Download All Images
============================================================ */

async function start() {
    const browser = await puppeteer.launch({
        args: ['--proxy-server="direct://"', '--proxy-bypass-list=*'],
        ignoreHTTPSErrors: true,
        headless: false,
        defaultViewport: null,
        timeout: 300000
    });
    const page = await browser.newPage();

    await page.goto('https://www.ddteedin.com/register/');
    await page.waitForTimeout(3000);


    /* ============================================================
       Select mode to get image
    ============================================================ */
    const destinationPath = '../images/'
    const imgName = 'captcha'
    const imgExtension = '.png'
    const fullImgPath = destinationPath + imgName + imgExtension

    // MODE : 1 (Download image)
    // MODE : 2 (Screenshot)

    switch (process.argv[2]) {
        case "1":
            const captchaTexts = await page.$$eval('img[src*="	https://www.ddteedin.com/captcha/"]', imgs => imgs.map(img => img.getAttribute('src')));
            await download(captchaTexts[0], fullImgPath);
            break;
        case "2":
            await printScreen(page, '#captcha', fullImgPath);
            break;
        default:
            await printScreen(page, '#captcha', fullImgPath);
            break;
    }

    /* ============================================================
       Captcha solver
    ============================================================ */
    let captcha = await captchaSolver(num_case, fullImgPath);

    await page.waitForTimeout(3000);

    console.log("captcha = ", captcha)

    await browser.close();
};

/* ============================================================
  Promise-Based Download Function
============================================================ */

const download = (url, destination) => new Promise((resolve, reject) => {
    var resultHandler = function(err) {
        if (err) {
            console.log("unlink failed", err);
        } else {
            console.log("file deleted");
        }
    }
    fs.unlink(destination, resultHandler);
    // existing file removed...

    const file = fs.createWriteStream(destination, { flag: "wx" });

    https.get(url, response => {
        response.pipe(file);

        file.on('finish', () => {
            file.close(resolve(true));
        });
    }).on('error', error => {
        fs.unlink(destination);

        reject(error.message);
    });
});

async function printScreen(page, selector, destination) {
    const logo = await page.$(selector); // declare a variable with an ElementHandle
    const box = await logo.boundingBox(); // this method returns an array of geometric parameters of the element in pixels.
    const x = box['x']; // coordinate x
    const y = box['y']; // coordinate y
    const w = box['width']; // area width
    const h = box['height']; // area height
    await page.waitForTimeout(3000);

    await page.screenshot({ path: destination, 'clip': { 'x': x, 'y': y, 'width': w, 'height': h } }); // take screenshot of the required area in puppeteer

}

async function captchaSolver(whitelist, path) {
    let result = '';
    await tesseract.recognize(path, {
            lang: "eng",
            psm: 1,
            load_system_dawg: false,
            tessedit_char_whitelist: `'${whitelist}'`,
            presets: ["txt"],
        })
        .then((text) => {
            result = text.split('\n')[0];
        })
        .catch((error) => {
            console.log(error.message)
        })

    return result
}

start();